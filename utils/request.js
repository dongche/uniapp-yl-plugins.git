/**
 * 处理返回结果
 * @param res
 * @param resolve
 * @param reject
 */
const handleRequestResult=(res,resolve,reject)=>{
	let {errmsg,errno,data}=res.data
	if(errno !==0){
		console.log('网络错误')
		uni.showToast({
			title:errmsg || '网络错误！请稍后重试。',
			icon:'none'
		})
		if(errno===501){
			//重新登录
			goLogin()
		}
		reject(errmsg)
	}else{
		let resData=data || {msg:errmsg}
		resolve(resData)
	}
}
/**
 * 请求的回调函数
 * @type {{fail(*=): void, success: (function(*=): void), complete(): void}}
 */
const requestCallback=(resolve,reject)=>{
	return {
		success: (res) => {
			//?返回的是一个字符串？
			// console.log(res);
			uni.hideLoading()
			return handleRequestResult(res,resolve,reject)
		},
		fail(err) {
			uni.hideLoading()
			console.log(err)
			let {errno,errmsg}=err.data || {}
			uni.showToast({title:errmsg || '网络错误',icon:'error'})
			if(errno===501){
				//重新登录
				store.dispatch('setLoginInfo',{})
				uni.removeStorageSync('userInfo')
				uni.removeStorageSync('hasLogin')
				uni.reLaunch({
					url: '/pages/login/login'
				})
			}
			reject(err)
		},
	}
}
const request = ({ url, data = {}, method = 'GET', header = {}, configs}) => {
	return new Promise(async (resolve,reject) => {
		header = {
			'content-type': 'application/json; charset=UTF-8',
			'X-Litemall-Token': uni.getStorageSync('token'),
			...header,
		}
		uni.request({
			url: "https://test.cn/wx/" + url,
			header,
			method,
			data,
			...requestCallback(resolve,reject)
		})
	})
}



export {
	request
}
